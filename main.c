#include <stdio.h>
#include <malloc.h>

int scalar_product(int *arr1, int *arr2, int len)
{
    int i = 0;
    int res = 0;
    for (i = 0; i < len; i++)
    {
        res += arr1[i] * arr2[i];
    }
    return res;
}

int is_prime( unsigned long n )
{
    int i;
    if (n>=2){
    for (i = 2; i <= n/2; i++)
    {
        if (n % i == 0)
        {
            return 0;
        }
    }
    } else {
        return 0;
    }
    return 1;
}

int main(int argc, char** argv)
{
    unsigned long number;
    int res, len, i;
    int *arr1, *arr2;
    printf("Size of arrays: ");
    scanf("%d", &len);
    arr1 = malloc(sizeof(int) * len);
    printf("1st vector: ");
    for (i = 0; i < len; i ++)
    {
        scanf("%d", &arr1[i]);
    }
    arr2 = malloc(sizeof(int) * len);
    printf("2nd vector: ");
    for (i = 0; i < len; i ++)
    {
        scanf("%d", &arr2[i]);
    }
    printf("Scalar product: %d\n", scalar_product(arr1, arr2, len));
    printf("Your number: ");
    scanf("%lu", &number);
    res = is_prime(number);
    if (res == 1)
    {
        printf("%lu is prime\n", number);
    }
    else 
    {
        printf("%lu is not prime\n", number);
    }
    return 0;
}
